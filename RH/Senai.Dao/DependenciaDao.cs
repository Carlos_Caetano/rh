﻿using RH.Senai.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RH.Senai.Dao
{
    class DependenciaDao : IDao<Dependencia>
    {

        // atributos
        private SqlConnection connection;

        // instruçao sql
        private string sql = null;

        // mensagem do messagebox
        private string msg = null;

        // titulo do messagebox
        private string titulo = null;

        // construtor
        public DependenciaDao()
        {
            // cria uma conexão com o banco de dados
            connection = new ConnecctionFactory().GetConnection();
        }

        public List<Dependencia> Consultar()
        {
            // lista de funcionarios cadastrados
            sql = "SELECT * FROM Dependencia";
            List<Dependencia> dependencias = new List<Dependencia>();
            try
            {
                // abre a conexao com o bando de dados
                connection.Open();
                // comando sql
                SqlCommand cmd = new SqlCommand(sql, connection);

                SqlDataReader leitor = cmd.ExecuteReader();

                // enquanto o leitor tiver dados para ler
                while (leitor.Read())
                {
                    Dependencia dependencia = new Dependencia();
                    dependencia.Id = (long)leitor["IDDependencia"];
                    dependencia.Descricao = leitor["Descricao"].ToString();

                    // adiciona funcionario na lista de funcionarios
                    dependencias.Add(dependencia);
                }// fim do while
            }
            catch (Exception ex)
            {

                msg = "erro ao consultar os funcionarios cadastrados" + ex.Message;
                titulo = "Erro...";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                connection.Close();
            }
            return dependencias;
            
        }

        public Dependencia Consultar(string parametro)
        {
            throw new NotImplementedException();
        }

        public void Exlcuir(Dependencia t)
        {
            throw new NotImplementedException();
        }

        public void Salvar(Dependencia dependencia)
        {

            // verifica SE id do dependencia é diferente de 0
            //if (Dependencia != 0)


            if (dependencia.Id != 0)
            {
                // update
                sql = "UPDATE DEPENDENCIA SET Descricao=@Descricao, WHERE IDDepencia = @IDDependencia";
            }
            else
            {
                // insert
                sql = "INSERT INTO DEPENDENCIA(Descricao) VALUES(@Descricao)";
            }
            try
            {

                // abre uma conexão com o banco de dados
                connection.Open();

                // comando sql 
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.Parameters.AddWithValue("@IDDependencia", dependencia.Id);
                cmd.Parameters.AddWithValue("@Descricao", dependencia.Descricao);

                // executa o INSERT
                cmd.ExecuteNonQuery();
                msg = "Dependencia " + dependencia.Descricao + "salvo com sucesso!";
                titulo = "Sycesso...";

                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (SqlException ex)
            {
                msg = "Erro ao Salvar dependente ! " + ex;
                titulo = "Erro";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                connection.Close();
            }
        }

    }

}
