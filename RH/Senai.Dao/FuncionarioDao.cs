﻿using RH.Senai.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RH.Senai.Dao
{
    class FuncionarioDao : IDao<Funcionario>
    {

        // atributos
        private SqlConnection connection;

        // instruçao sql
        private string sql = null;

        // mensagem do messagebox
        private string msg = null;

        // titulo do messagebox
        private string titulo = null;

        // construtor
        public FuncionarioDao()
        {
            // cria uma conexão com o banco de dados
            connection = new ConnecctionFactory().GetConnection();
        }

        public Funcionario Consultar(string parametro)
        {
            // instruçao sql
            sql = "SELECT * FROM Funcionario WHERE CPF = @Cpf";

            Funcionario funcionario = null;

            try
            {
                //abre a conexão com o banco de dados
                connection.Open();

                // comando sql
                SqlCommand cmd = new SqlCommand(sql, connection);

                // parametro do comando sql
                cmd.Parameters.AddWithValue("@Cpf", parametro);
                // leitor de dados sql
                // recebe os dados do banco de dados
                SqlDataReader leitor = cmd.ExecuteReader();

                

                while (leitor.Read())
                {
                    // compara se o cpf dos registros é igual
                    // ao cpf de parâmetro

                    if (parametro.Equals(leitor["cpf"].ToString() ))
                    {
                        // cira o funcionario
                        funcionario = new Funcionario();
                       
                        funcionario.id = (long)leitor["IDFuncionario"];
                        funcionario.Nome = leitor["Nome"].ToString();
                        funcionario.Cpf = leitor["Cpf"].ToString();
                        funcionario.RG = leitor["RG"].ToString();
                        funcionario.Email = leitor["Email"].ToString();
                        funcionario.Telefone = leitor["Telefone"].ToString();

                    } // fim do if
                } // fim do while



            }
            catch (SqlException ex)
            {

                msg = "Erro ao consultar o funcionario !" + ex.Message;
                titulo = "Erro...";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);


            }
            finally
            {
                connection.Close();
            }
            // retorna o funcionario
            return funcionario;
        }


        // metodos 
        public List<Funcionario> Consultar()
        {
            // lista de funcionarios cadastrados
            sql = "SELECT * FROM Funcionario";
            List<Funcionario> funcionarios = new List<Funcionario>();
            try
            {
                // abre a conexao com o bando de dados
                connection.Open();
                // comando sql
                SqlCommand cmd = new SqlCommand(sql, connection);

                SqlDataReader leitor = cmd.ExecuteReader();

                // enquanto o leitor tiver dados para ler
                while (leitor.Read())
                {
                    Funcionario funcionario = new Funcionario();
                    funcionario.id = (long) leitor["IDFuncionario"];
                    funcionario.Nome = leitor["Nome"].ToString();
                    funcionario.Cpf = leitor["Cpf"].ToString();
                    funcionario.RG = leitor["RG"].ToString();
                    funcionario.Email = leitor["Email"].ToString();
                    funcionario.Telefone = leitor["Telefone"].ToString();
                    // adiciona funcionario na lista de funcionarios
                    funcionarios.Add(funcionario);
                }// fim do while
            }
            catch (Exception ex)
            {

                msg = "erro ao consultar os funcionarios cadastrados" + ex.Message;
                titulo = "Erro...";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                connection.Close();
            }
            return funcionarios;
        }

        public void Exlcuir(Funcionario funcionario)
        {
            // instrução sql
            sql = "DELETE FROM Funcionario WHERE IDFuncionario = @idfuncionario";
            try
            {
                // abre a conexão com o banco de dados
                connection.Open();

                // cria um comando sql
                SqlCommand cmd = new SqlCommand(sql, connection);

                // adiciona valor ao parâmetro @idFuncionario

                cmd.Parameters.AddWithValue("@idfuncionario", funcionario.id);

                // executa o comando sql no banco de dados
                cmd.ExecuteNonQuery();

                // mensagem de feedback
                msg = "Funcionario" + funcionario.Nome + "excluido com sucesso";

                // titulo da mensagem de feedback
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (SqlException ex)
            {
                // mensagem de fodback 
                msg = "Erro ao excluir o funcionário !" + ex;
                // titulo de mensagem de erro
                titulo = "Erro...";

                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }  
            finally
            {
                connection.Close();
            }
        }

        public void Salvar(Funcionario funcionario)
        {
            // verifica SE id do funcionário é diferente de 0
            if (funcionario.id != 0)
            {
                // update
                sql = "UPDATE FUNCIONARIO SET Nome=@Nome, Cpf=@Cpf, Rg=@Rg, Email=@Email, Telefone=@Telefone WHERE idFuncionario = @idFuncionario";
            }
            else
            {
                // insert
                sql = "INSERT INTO FUNCIONARIO(Nome, Cpf, Rg, Email, Telefone) VALUES(@Nome, @Cpf,@Rg, @Email,@Telefone)";
            }
            try
            {
               
                // abre uma conexão com o banco de dados
                connection.Open();

                // comando sql 
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.Parameters.AddWithValue("@IDFuncionario", funcionario.id);
                cmd.Parameters.AddWithValue("@Nome", funcionario.Nome);
                cmd.Parameters.AddWithValue("@Cpf", funcionario.Cpf);
                cmd.Parameters.AddWithValue("@Rg", funcionario.RG);
                cmd.Parameters.AddWithValue("@Email", funcionario.Email);
                cmd.Parameters.AddWithValue("@Telefone", funcionario.Telefone);

                // executa o INSERT
                cmd.ExecuteNonQuery();
                msg = "Funcionario " + funcionario.Nome + "salvo com sucesso!";
                titulo = "Sycesso...";

                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch(SqlException ex)
            {
                msg = "Erro ao Salvar funcionário ! " + ex;
                titulo = "Erro";
                MessageBox.Show(msg , titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
