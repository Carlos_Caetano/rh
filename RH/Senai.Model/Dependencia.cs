﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RH.Senai.Model
{
    class Dependencia
    {
        private long id;

        public long Id
        {
            get { return id; }
            set { id = value; }
        }
        private string descricao;

        public string Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }
        public Dependencia()
        {

        }
        public Dependencia(long id, string descricao)
        {
            this.id = id;
            this.descricao = descricao;
        }
    }
}
