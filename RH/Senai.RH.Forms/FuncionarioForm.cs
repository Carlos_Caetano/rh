﻿using RH.Senai.Dao;
using RH.Senai.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RH.Senai.RH.Forms
{
    public partial class FuncionarioForm : Form
    {
        public FuncionarioForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("CPF: " + TxtCpfFuncionario.Text);
        }

        private void btnSalvarFuncionario_Click(object sender, EventArgs e)
        {
            // validação
            // verifica se os campos obrigatórios foram 
            // preenchidos

            if (string.IsNullOrEmpty(txtNomeFuncionario.Text)
                && string.IsNullOrEmpty(TxtCpfFuncionario.Text)
                && string.IsNullOrEmpty(TxtCpfFuncionario.Text)
                && string.IsNullOrEmpty(txtEmailFuncionario.Text)
                && string.IsNullOrEmpty(txtTelefoneFuncionario.Text))
            {
                MessageBox.Show("Preencha todos os campos");
            }
            else
            {

                // intancia um funcionário
                Funcionario funcionario = new Funcionario();

                // obtém o id do funcionário
                // Se o TextBox de Id de funcionario Não estiver vazio
                // se o foi preenchido o TexBox de IDFuncionario

                if (!string.IsNullOrEmpty(txtIDFuncionario.Text))
                {
                    // cria um id
                    long id = 0;
                    // converte o texto do TextBox para long
                    // armazenando na variável id
                    if (long.TryParse(txtIDFuncionario.Text, out id))
                    {
                        // atribui o id do objeto funcionário
                        funcionario.id = id;
                    }
                }

                // atribui dados ao funcionário

                funcionario.Nome = txtNomeFuncionario.Text;
                funcionario.Cpf = TxtCpfFuncionario.Text;
                funcionario.RG = txtRGFuncionario.Text;
                funcionario.Email = txtEmailFuncionario.Text;
                funcionario.Telefone = txtTelefoneFuncionario.Text;

                // instancia o dao de funcionarios
                FuncionarioDao dao = new FuncionarioDao();
                // salva funcionairo no banco de dados
                dao.Salvar(funcionario);

                // atualiza o data grid view
                PreencherDados();

                LimparFormulario();
            }
        }    
            // fim do evento de click

        // métodos do programador
        private void LimparFormulario()
        {
            txtIDFuncionario.Clear();
            txtNomeFuncionario.Clear();
            TxtCpfFuncionario.Clear();
            txtRGFuncionario.Clear();
            txtEmailFuncionario.Clear();
            txtTelefoneFuncionario.Clear();
            TxtCpfFuncionario.Focus();
        }

        private void FuncionarioForm_Load(object sender, EventArgs e)
        {
            PreencherDados();
        }

        private void PreencherDados()
        {

            // instancio uma dao
           // FuncionarioDao dao = new FuncionarioDao();

            // preenche o data grid view
            dgvFuncionarios.DataSource = new FuncionarioDao().Consultar();


            // oculta algumas colunas
            dgvFuncionarios.Columns["ID"].Visible = false;
            dgvFuncionarios.Columns["Rg"].Visible = false;

            // limpa a seleção do data grid view
            dgvFuncionarios.ClearSelection();

            // limpa os campso do forumlário
            LimparFormulario();

        }

        private void dgvFuncionarios_SelectionChanged(object sender, EventArgs e)
        {
            // se alguma linha for selecionada
            if (dgvFuncionarios.CurrentRow != null)
            {
                // pega o id e coloca no textbox de id
                txtIDFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[0].Value.ToString();
                txtNomeFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[1].Value.ToString();
                TxtCpfFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[2].Value.ToString();
                txtRGFuncionario.Text =  dgvFuncionarios.CurrentRow.Cells[3].Value.ToString();
                txtEmailFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[4].Value.ToString();
                txtTelefoneFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[5].Value.ToString();
            }
        }

        private void dgvFuncionarios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnExcluirFuncionario_Click(object sender, EventArgs e)
        {
            // verifica se o TextBox de ID de funcionário é nulo ou 
            // vazio
            // se sim. Isso significa que nenhum funcionario foi
            // selecionado na lista

            if (string.IsNullOrEmpty(txtIDFuncionario.Text))
            {
                string msg = "Selecione um funcionario na lista abaixo !";
                string titulo = "Operação não realizada...";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                // instancia um funcionario
                Funcionario funcionario = new Funcionario();

                // cria um id para o funcionário
                long id = 0;

                if (long.TryParse(txtEmailFuncionario.Text, out id))
                {
                    // atribui o id ao id do funcionario
                    funcionario.id = id;
                }

                // instancia um dao
                FuncionarioDao dao = new FuncionarioDao();

                // resposta do usuário
                DialogResult resposta = MessageBox.Show("Deseja realmente excluir essse funcionario?" , "Mensagem...",MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (resposta.Equals(DialogResult.Yes))
                {
                    // exclui o funcionario
                    dao.Exlcuir(funcionario);

                    // atualiza
                    PreencherDados();
                } 
            }
        }

        private void TxtCpfFuncionario_Leave(object sender, EventArgs e)
        {
            // pega o texto do textbox de cpf
            string cpf = TxtCpfFuncionario.Text;

            // cria um funcionário
            Funcionario funcionario = new FuncionarioDao().Consultar(cpf);

            // verifica se funcionario não é nulo
            if (funcionario != null)
            {
                // prenche os campos do form
                txtIDFuncionario.Text = funcionario.id.ToString();
                txtNomeFuncionario.Text = funcionario.Nome;
                TxtCpfFuncionario.Text = funcionario.Cpf;
                txtRGFuncionario.Text = funcionario.RG;
                txtEmailFuncionario.Text = funcionario.Email;
                txtTelefoneFuncionario.Text = funcionario.Telefone;

            }
        }
    }// fim da classe 

}
