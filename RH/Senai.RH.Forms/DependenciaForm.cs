﻿using RH.Senai.Dao;
using RH.Senai.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RH.Senai.RH.Forms
{
    public partial class DependenciaForm : Form
    {
        public DependenciaForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            // intancia um dependencia
            Dependencia dependencia = new Dependencia();

            // obtém o id do dependencia
            // Se o TextBox de Id de funcionario Não estiver vazio
            // se o foi preenchido o TexBox de IDDependencia

            if (!string.IsNullOrEmpty(txtIDDependencia.Text))
            {
                // cria um id
                long id = 0;
                // converte o texto do TextBox para long
                // armazenando na variável id
                if (long.TryParse(txtIDDependencia.Text, out id))
                {
                    // atribui o id do objeto funcionário
                    dependencia.Id = id;
                }
            }

            // atribui dados ao funcionário

            dependencia.Descricao = txtDescricao.Text;


            // instancia o dao de funcionarios
            DependenciaDao dao = new DependenciaDao();
            // salva funcionairo no banco de dados
            dao.Salvar(dependencia);

            LimparFormulario();

            PreencherDados();
        }

        // métodos do programador
        private void LimparFormulario()
        {
            txtIDDependencia.Clear();
            txtDescricao.Clear();
        }


        private void PreencherDados()
        {
            // instancio uma dao
            // DependenciaDao dao = new DependenciaDao();

            // preenche o data grid view
            DgvDependencia.DataSource = new DependenciaDao().Consultar();




            // limpa a seleção do data grid view
            DgvDependencia.ClearSelection();

            // limpa os campso do forumlário
            LimparFormulario();
        }

        private void DependenciaForm_Load(object sender, EventArgs e)
        {
            PreencherDados();
        }
    }
     
    

        

    }

